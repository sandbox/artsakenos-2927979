<?php

namespace Drupal\spid_auth\EventSubscriber;
 
use Drupal\spid_auth\Exceptions\SpidException;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\Url;
/**
 * Redirect .html pages to corresponding Node page.
 */
class SpidAuthSubscriber implements EventSubscriberInterface {
 
  /** @var int */
  private $redirectCode = 301;



  public function handleSpidException(GetResponseForExceptionEvent $event){
      $exception = $event->getException();
      if ($exception instanceof SpidException) {
          $request = $event->getRequest();
          drupal_set_message(t('We\'re sorry. There was a problem. The issue has been logged for the administrator.') , 'error');
          watchdog_exception("ERROR", $exception);
          $response = new RedirectResponse('/servizi-online');
          $response->send();
      }
  }
 
  /**
   * Listen to kernel.request events and call customRedirection.
   * {@inheritdoc}
   * @return array Event names to listen to (key) and methods to call (value)
   */
  public static function getSubscribedEvents() {
      $events[KernelEvents::EXCEPTION] = ['handleSpidException'];
    return $events;
  }
}
