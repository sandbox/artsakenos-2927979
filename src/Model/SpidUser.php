<?php
/**
 * Created by PhpStorm.
 * User: drupaldev
 * Date: 27/11/17
 * Time: 18.03
 */

namespace Drupal\spid_auth\Model;


use DateTime;

class SpidUser
{

    private $gender = "S", $familyName="TEST_FAMILYNAME", $name = "TEST_NAME", $spidCode="TEST_SPIDCODE", $dateOfBirth=NULL, $fiscalNumber= "TEST_FISCALNUMBER", $email="TEST_EMAIL";



//$gender, $familyName, $name, $spidCode, $dateOfBirth, $fiscalNumber, $email

    public function __construct(array $attributes)
    {
        if($attributes){
            $this->gender = reset($attributes['gender']);
            $this->familyName = reset($attributes['familyName']);
            $this->name = reset($attributes['name']);
            $this->spidCode = reset($attributes['spidCode']);
            $this->dateOfBirth = DateTime::createFromFormat('Y-m-d', reset($attributes['dateOfBirth']));
            $this->fiscalNumber = explode('-', reset($attributes['fiscalNumber']))[1];
            $this->email = reset($attributes['email']);
        }
        else{
            $this->dateOfBirth = DateTime::createFromFormat('Y-m-d', '1900-01-01');
        }

    }

    /**
     * @return String
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @return String
     */
    public function getFamilyName()
    {
        return $this->familyName;
    }

    /**
     * @return String
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return String
     */
    public function getSpidCode()
    {
        return $this->spidCode;
    }


    /**
     * @return bool|DateTime
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * @return String
     */
    public function getFiscalNumber()
    {
        return $this->fiscalNumber;
    }

    /**
     * @return String
     */
    public function getEmail()
    {
        return $this->email;
    }


}