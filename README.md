
INTRODUZIONE
============
Il codice è stato sviluppato a partire da:
https://github.com/retepasw/drupal-spid         version = "7.x-0.3"     (pasw)
    utilizzabile con il pacchetto spidinst, e
https://github.com/ComuneFI/spid-drupal         version = "7.x-beta1"   (comune fi)

Documentazione di riferimento
  http://www.agid.gov.it/sites/default/files/circolari/spid-regole_tecniche_v1.pdf
  http://www.agid.gov.it/sites/default/files/regole_tecniche/tabella_attributi_idp_v1_0.pdf

UTILIZZO
========
Installare il modulo e configurarlo dalla pagina:
    /admin/config/social-api/spid-auth
o dal path
    Configurazione > Social API Settings > Impostazioni di Spid Auth

NOTE DI SVILUPPO
================
Controllare i TODO all'interno del codice.
