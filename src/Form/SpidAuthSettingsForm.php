<?php

namespace Drupal\spid_auth\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class SpidAuthSettingsForm extends ConfigFormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'spid_auth_admin_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            'spid_auth.settings',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config('spid_auth.settings');

        $lib_path = libraries_get_path('simplespidphp');
        // TODO, Fare un'installer automatico per la copia delle librerie nel loro path naturale.
        //       Controllare anche se le librerie sono configurate, non solo installate, e scrivere delle indicazioni.
        $lib_description = "Attenzione, le librerie non sono installate, per installarle si prega di visitare https://github.com/artsakenos/simplespidphp";
        if ($lib_path) {
            $lib_description = "Le librerie sono correttamente installate nel percorso indicato.";
        }
        $form['spid_auth_librerie'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Librerie per i Servizi Spid'),
            '#description' => $this->t($lib_description),
            '#default_value' => $lib_path,
            '#disabled' => TRUE,
        );

        $form['spid_auth_id'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Spid Auth ID'),
            '#description' => $this->t('ID della configurazione dei servizi Spid, e.g., default-sp'),
            '#default_value' => $config->get('spid_auth_id'),
        );

        $form['spid_auth_servizi_spid'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Pagina dei Servizi Spid'),
            '#description' => $this->t('La landing page dopo il login Spid ai servizi. Inserire il percorso interno, e.g., /servizi-online'),
            '#default_value' => $config->get('spid_auth_servizi_spid'),
        );

        $form['spid_auth_debug'] = array(
            '#type' => 'checkbox',
            '#title' => $this->t('Spid Auth Debug'),
            '#description' => $this->t('Attiva alcune funzionalità e log di debug'),
            '#default_value' => $config->get('spid_auth_debug'),
        );

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        // Retrieve the configuration
        \Drupal::configFactory()->getEditable('spid_auth.settings')
                // Set the submitted configuration setting
                ->set('spid_auth_servizi_spid', $form_state->getValue('spid_auth_servizi_spid'))
                ->set('spid_auth_debug', $form_state->getValue('spid_auth_debug'))
                ->save();

        parent::submitForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        $servizi_spid = $form_state->getValue('spid_auth_servizi_spid');
        // Se la pagina non esiste, restituisce un errore.
        if (!\Drupal::service('path.validator')->isValid($servizi_spid)) {
            $form_state->setErrorByName('spid_auth_servizi_spid', $this->t('Attenzione, il path selezionato non esiste.'));
        }
    }

}
