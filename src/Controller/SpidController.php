<?php

/**
 * @file
 * Contains \Drupal\spid_auth\Controller\SpidController.
 *
 * Implementa le callback e azioni legate all'autenticazione con SPID.
 */

namespace Drupal\spid_auth\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Drupal\spid_auth\Exceptions\SpidException;
use Drupal\user\Entity\User;
use SimpleSAML_Auth_Simple;
use Symfony\Component\HttpFoundation\RedirectResponse;

require_once(libraries_get_path('simplespidphp') . '/lib/_autoload.php');
require_once(libraries_get_path('simplespidphp') . '/lib/_autoload_modules.php');

class SpidController extends ControllerBase {

    private $_simplesamlphp_auth_as;
    private $_spid_config;

    public function __construct() {
        $this->_spid_config = \Drupal::config('spid_auth.settings');
        $this->_simplesamlphp_auth_as = new \SimpleSAML_Auth_Simple($this->_spid_config->get('spid_auth_id'));
    }

    /**
     * @return array
     * @throws SpidException
     *
     */
    public function spidLogin() {

        // Do some sanity checking before attempting anything.
        $config = \SimpleSAML_Configuration::getInstance();
        $configStoreType = $config->getValue('store.type');

        // Make sure phpsession is NOT being used.
        if ($configStoreType == 'phpsession') {
            // 'A user attempted to login using simplesamlphp but the store.type is phpsession, use memcache or sql for simplesamlphp session storage. See: simplesamlphp/config/config.php.'
            throw new SpidException('Un utente ha tentato il login tramite simplesamlphp ma store.type è uguale a phpsession. Utilizzare memcache o sql per la simplesamlphp session storage. vedi: simplespidphp/config/config.php.');
        }

        if (!isset($_REQUEST['idpentityid'])) {
            // 'Error: parameter idpentityid missing. May be an attack attempt or a misconfiguration in the spid form'
            throw new SpidException('Spid Error: Il parametro idpentityid è mancante. Si sta tentando di chiamare la callback manualmente?');
        }

        $options['saml:idp'] = $_REQUEST['idpentityid'];
        // Require the user to be authenticated.

        $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
        \Drupal::service('page_cache_kill_switch')->trigger();
        $authformat = 'https://www.spid.gov.it/%s';
        $authlevel = 'SpidL1'; //TODO mettere su configurazione
        //$options['saml:AuthnContextClassRef'] =  sprintf($authformat, $authlevel);
        $options['samlp:RequestedAuthnContext'] = array("Comparison" => "minimum");
        $options['ReturnTo'] = Url::fromRoute("spid_auth.saml_success", [], ['absolute' => TRUE])->toString(); // '/spid-login-success';
        $options['ErrorURL'] = Url::fromRoute("spid_auth.saml_error", [], ['absolute' => TRUE])->toString(); // '/spid-login-error';
        $this->_simplesamlphp_auth_as->requireAuth($options);

        return array(
            '#type' => 'markup',
            '#markup' => "Malfunzionamento temporaneo del servizio; riprova più tardi.",
            '#cache' => array('max-age' => 0)
        );
    }

    /**
     * If a drupal user must be created, should be here
     *
     */
    public function spidLoginSuccess() {
        // Prendiamo il primo utente con ruolo SPID e lo logghiamo

        \Drupal::service('page_cache_kill_switch')->trigger();

        // $servizi_spid_url = "internal:/servizi-online";
        $servizi_spid_url = "internal:" . $this->_spid_config->get('spid_auth_servizi_spid');

        $user = \Drupal::currentUser();
        if ($user && $user->isAuthenticated()) {
            $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
            $response = new RedirectResponse(Url::fromUri($servizi_spid_url)->toString());
            $response->send();
            return $response;
        }

        $spidUser = spid_auth_get_logged_user();
        if (!$spidUser) {
            throw new SpidException("L'utente spid non è correntemente definito");
        }

        $ids = \Drupal::entityQuery('user')
                ->condition('status', 1)
                ->condition('roles', 'utente_spid')
                ->execute();
        $users = User::loadMultiple($ids);
        $user = reset($users);
        user_login_finalize($user);

        $response = new RedirectResponse(Url::fromUri($servizi_spid_url)->toString());
        return $response;
    }

    public function spidLoginError() {


        $exceptionId = $_REQUEST['SimpleSAML_Auth_State_exceptionId'];

        if ($exceptionId) {
            $state = \SimpleSAML_Auth_State::loadExceptionState($exceptionId);
        }


        \Drupal::logger("spid_auth")->error("Errore provider<br />.<br>Eccezione: " . $state['SimpleSAML_Auth_State.exceptionData']->getStatusMessage());

        return array(
            '#type' => 'markup',
            '#markup' => "Si è verificato un errore nella richiesta lato provider."
        );
    }

    public function spidLogout() {
        \Drupal::service('page_cache_kill_switch')->trigger();
        $user = \Drupal::currentUser();
        if (!$user || !$user->isAuthenticated()) {
            \Drupal::logger('spid_auth')->error('Tentativo di effettuare logout da SPID senza un utente loggato');
        }

        $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $returnUrl = Url::fromRoute("user.logout", array('language' => $language));
        $returnUrl->setAbsolute(TRUE);

        $this->_simplesamlphp_auth_as->logout(array(
            'ReturnTo' => $returnUrl->toString(),
            'ReturnStateParam' => 'LogoutState',
            'ReturnStateStage' => 'MyLogoutState'));
    }

}
