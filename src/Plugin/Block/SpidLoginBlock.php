<?php

namespace Drupal\spid_auth\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\user\Entity\User;

/**
 *
 * @Block(
 *   id = "spid_login_block",
 *   admin_label = @Translation("SPID login"),
 *   category = @Translation("SPID"),
 * )
 */
class SpidLoginBlock extends BlockBase {

    private static function getProviders() {
        // TODO: L'unico punto in cui veniva utilizzata la costante per il percorso delle librerie era questo:
        //  Il comportamento corretto sarebbe recuperare il percorso delle librerie con libraries_get_path('simplespidphp')
        //  E seguire le istruzioni in configurazione, perché le librerie vanno messe là. Lascio il TODO per discuterne.
        include('libraries/simplespidphp' . '/metadata/saml20-idp-remote.php');
        $providers = array();
        $providers['infocertid'] = array('path' => 'https://identity.infocert.it', 'name' => 'Infocert ID');
        $providers['timid'] = array('path' => 'https://login.id.tim.it/affwebservices/public/saml2sso', 'name' => 'TIM ID');
        $providers['posteid'] = array('path' => 'https://posteid.poste.it', 'name' => 'Poste ID');
        $providers['arubaid'] = array('path' => 'https://loginspid.aruba.it', 'name' => 'Infocert ID');
        $providers['namirialid'] = array('path' => 'https://idp.namirialtsp.com/idp', 'name' => 'Namirial ID');
        $providers['spiditalia'] = array('path' => 'https://spid.register.it', 'name' => 'SpidItalia ID');
        $providers['sielteid'] = array('path' => isset($metadata['identity.sieltecloud.it']) ? 'identity.sieltecloud.it' : 'https://identity.sieltecloud.it', 'name' => 'Sielte ID'); //non è chiaro il motivo di questo controllo, nel mosulo D7 c'era e ce lo teniamo

        return $providers;
    }

    /**
     * {@inheritdoc}
     */
    public function build() {

        $providers = self::getProviders();

        $formaction = '/spid-login';

        $spid_auth_path = drupal_get_path('module', "spid_auth");

        return array(
            //'#markup' => $spid_bottone,
            '#attached' => array('library' => ['spid_auth/loginbutton']),
            '#theme' => 'spid_auth_button',
            '#providers' => $providers,
            '#action' => $formaction,
            '#icon_path' => "/$spid_auth_path/vendor/agid/img/",
            '#cache' => array('max-age' => 0)
        );
    }

}
