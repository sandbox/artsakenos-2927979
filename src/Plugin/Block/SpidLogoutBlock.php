<?php

namespace Drupal\spid_auth\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 *
 * @Block(
 *   id = "spid_logout_block",
 *   admin_label = @Translation("SPID logout"),
 *   category = @Translation("SPID"),
 * )
 */
class SpidLogoutBlock extends BlockBase {




    /**
     * {@inheritdoc}
     */
    public function build() {


        $spidUser = spid_auth_get_logged_user();
        if($spidUser){
            return array(
                '#theme'=> "spid_auth_user",
                '#user' => $spidUser,
                '#cache' => array('max-age' => 0)
                );
        }
        return array();
    }

}